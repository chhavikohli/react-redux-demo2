import React, { Component } from "react";
import { connect } from "react-redux";
import uuidv1 from "uuid";
import { addArticle } from "../actions/index";
const mapDispatchToProps = dispatch => {
    return {
        addArticle: article => dispatch(addArticle(article))
    };
};
class ConnectedForm extends Component {
    constructor() {
        super();
        this.state = {
            title: "",
            description:"",
            price:"",
            image:""
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }
    handleSubmit(event) {
        event.preventDefault();
        const { title } = this.state;
        const { description } = this.state;
        const { price } = this.state;
        const { image } = this.state;
        const id = uuidv1();
        this.props.addArticle({ title,description,price,image, id });
        this.setState({ title: "" });
        this.setState({ description: "" });
        this.setState({ price: "" });
        this.setState({ image: "" });
    }
    render() {
        const { title } = this.state;
        const {description} = this.state;
        const {price} = this.state;
        const{image}=this.state;
        return (
            <form onSubmit={this.handleSubmit}>
                <div className="add-article">
                    <h2>Add Article</h2>
                    <label htmlFor="title">Title</label>
                    <input
                        type="text"
                        id="title"
                        value={title}
                        onChange={this.handleChange}
                    /><br/>
                    <label htmlFor="description">Description</label>
                    <textarea
                        type="text"
                        id="description"
                        value={description}
                        onChange={this.handleChange}
                    /><br/>
                    <label htmlFor="Price">Price</label>
                    <input
                        type="number"
                        id="price"
                        value={price}
                        onChange={this.handleChange}
                    /><br/>
                    <input type="file" onChange={this.fileChangedHandler}/>

                    <br/>
                    <button type="submit" className="btn-success">
                        SAVE
                    </button>
                </div>

            </form>
        );
    }
}
const Form = connect(null, mapDispatchToProps)(ConnectedForm);
export default Form;