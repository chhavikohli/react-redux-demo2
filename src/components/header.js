import React, { Component } from 'react';
import '../App.css';

class Header extends Component {
  render() {
    return (
      <div className="App-header">
    <img className="App-icon" src={require('../images/ecart.jpg')}  alt="icon" />
      </div>
    );
  }
}

export default Header;
