import React from "react";
import {connect} from "react-redux";

const mapStateToProps = state => {
    return {articles: state.articles};
};

const ConnectedList = ({articles}) => (

    <table id="articles">
        <tbody>
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
        <tr>
            <td> test1</td>
            <td> test1.description</td>
            <td> $50 </td>
            <td> </td>
            <td> <button type="submit" className="btn-update">
                Update
            </button> <button type="submit" className="btn-delete">
                Delete
            </button></td>
        </tr>
        {articles.map(el => (
            <tr key={el.id}>
                <td> {el.title}</td>
                <td> {el.description}</td>
                <td> ${el.price} </td>
                <td> </td>
                <td> <button  className="btn-update">
                    Update
                </button> <button  className="btn-delete">
                    Delete
                </button></td>
            </tr>
        ))}

        </tbody>


    </table>

);
const List = connect(mapStateToProps)(ConnectedList);
export default List;