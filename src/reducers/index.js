import {ADD_ARTICLE} from "../constants/action-types";
import {DELETE_ARTICLE} from "../constants/action-types";
import {UPDATE_ARTICLE} from "../constants/action-types";

const initialState = {
    articles: []
};
const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_ARTICLE:{
            return {...state, articles: [...state.articles, action.payload]};
        }

        case DELETE_ARTICLE:{
            return {...state, articles: state.articles.filter(articles => articles !== action.payload)};
        }
        case UPDATE_ARTICLE:{
            /*return {...state, articles: state.articles.filter(articles => articles !== action.payload)};*/
        }

        default:
            return state;
    }
};
export default rootReducer;