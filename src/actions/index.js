import {ADD_ARTICLE, UPDATE_ARTICLE} from "../constants/action-types";
import { DELETE_ARTICLE } from "../constants/action-types";

export const addArticle = article => ({ type: ADD_ARTICLE, payload: article });
export const deleteArticle = article => ({ type: DELETE_ARTICLE, payload: article });
export const updateArticle = article => ({ type: UPDATE_ARTICLE, payload: article });