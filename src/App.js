import React from "react";
import List from "./components/List";
import Form from "./components/Form";
import Header from './components/header';
import Footer from './components/footer';
import './App.css';

const App = () => (
    <div className="App">
        <div><Header/></div>
        <div className="App-Body">
            <div className="article">
                <h2>Existing Articles</h2>
                <List/>
            </div>
            <div className="new-article">
                <Form/>
            </div>
        </div>

        <div><Footer/></div>
    </div>

);
export default App;